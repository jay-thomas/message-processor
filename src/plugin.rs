use rune::termcolor::{ColorChoice, StandardStream};
use rune::EmitDiagnostics as _;
use runestick::{Vm, Item, Module, Source};
use std::error::{Error as StdError};
use std::fs::File;
use std::io::{Error as IoError, Read as _};
use std::string::String;
use std::sync::Arc;
use crate::types;

/// Instantiate Rune VM for plugins
pub fn new(source: String) -> Result<Vm, Box<dyn StdError>> {
    let mut module = Module::default();
    module.ty::<types::UserMessage>()?;
    let mut context = runestick::Context::with_default_modules()?;
    context.install(&module)?;
    let options = rune::Options::default();

    let mut sources = rune::Sources::new();
    sources.insert(Source::new("script", source));

    let mut diagnostics = rune::Diagnostics::new();

    let result = rune::load_sources(&context, &options, &mut sources, &mut diagnostics);

    if !diagnostics.is_empty() {
        let mut writer = StandardStream::stderr(ColorChoice::Always);
        diagnostics.emit_diagnostics(&mut writer, &sources)?;
    }

    let unit = result?;
    return Ok(Vm::new(Arc::new(context.runtime()), Arc::new(unit)));
}

pub fn from_file(filename: &str) -> Result<String, IoError> {
    let mut content = String::new();
    File::open(filename)?.read_to_string(&mut content)?;
    Ok(content)
}
