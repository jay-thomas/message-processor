use runestick::{Any};

#[derive(Any)]
pub struct UserMessage {
    #[rune(get, set, add_assign, copy)]
    pub id: i64,
    #[rune(get, set)]
    pub body: String
}

