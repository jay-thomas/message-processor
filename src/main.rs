use runestick::{FromValue as _};
use std::io;
use std::io::prelude::*;
use std::error::Error;
use std::process;

pub mod plugin;
pub mod types;


/*
impl Args for UserMessage {
    fn default() -> Self {
        Self {
            id: 123,
            body: String::from("test message")
        }
    }
}
*/

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let filename = "plugins/calculate.rn";
    let script = match plugin::from_file(filename) {
        Ok(s) => s,
        Err(_) => {
            println!("Could not open file \"{}\". Exiting.", filename);
            process::exit(1);
        }
    };
    let vm = match plugin::new(script) {
        Ok(v) => v,
        _ => {
            println!("Failed to load plugin. Exiting.");
            process::exit(1);
        }
    };

    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let user_message = types::UserMessage {
            id: 123,
            body: line.unwrap()
        };
        vm.clone().execute(&["on_message"], (user_message,))?.async_complete().await?;
    }

    /*
    let mut execution = vm.execute(&["calculate"], (10i64, 20i64))?;
    let value = execution.async_complete().await?;
    let value = i64::from_value(value)?;
    println!("{}", value);
    */

    Ok(())
}
